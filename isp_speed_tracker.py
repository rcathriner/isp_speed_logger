#!/usr/bin/python

import speedtest_cli as st
import pandas as pd
import matplotlib as matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse as ap
import datetime
import os

#setup argparse
parser = ap.ArgumentParser()
parser.add_argument('--showPlot', help='Flag, Enable a blocking command that displays the plotted data in a matplotlib figure window', action='store_true')
parser.add_argument('--noTest', help='Flag, Skip the speed test and data logging, read data and plot only', action='store_true')
parser.add_argument('--resultsFile', help='String, Specify the path to the CSV file in which results are stored to and read from', default='./testResults.csv')
parser.add_argument('--plotFile', help='String, Specify the path to the PNG file in which results will be plotted and saved', default='./connectionSummary.png')
args = parser.parse_args()

class ISPSpeedTracker:
    header = 't,ping_ms,downloadSpeed_bps,uploadSpeed_bps\n'
    rowTemplate = '{},{},{},{}\n'
    
    def __init__(self, fileName=None):
        if fileName is None:
            errstr = "ISPSpeedTracker may not be initialized without a file name"
            raise Exception(errstr)
        self.fn = fileName
        if not self.csvIsInit():
            self.initCSV()

    def runTest(self):
        resDict = st.speedtestValueReturn()
        t = datetime.datetime.now() 
        res =  [t,
                resDict['ping'],
                resDict['downloadSpeed'],
                resDict['uploadSpeed']
                ]
        return res
    
    def readResultsTable(self):
        resDF = pd.read_csv(fn,
                            header=0,
                            )
        return resDF
    
    def recordResults(self, results):
        with open(self.fn, 'a') as csv:
            rowStr = self.rowTemplate.format(results[0],
                                             results[1],
                                             results[2],
                                             results[3])
            csv.write(rowStr)
    
    def csvIsInit(self):
        if os.path.isfile(self.fn):
            with open(self.fn, 'r') as csv:
                if not csv:
                    errstr = "Checking initializaion failed"
                    raise Exception(errstr)
                
                filehead = csv.next()
            if filehead == self.header:
                return True
        return False
    
    def initCSV(self):
        with open(self.fn, 'w') as csv:
            if not csv:
                errstr = "Creating new file failed"
                raise Exception(errstr)

            #printing header and closing file
            csv.write(self.header)
    
    def clearCSV(self):
        open(self.fn, 'w').close()

    def plotResults(self, figName, showPlot):
        #read in data and rescale for plotting
        df = pd.read_csv(self.fn,
                         header=0,
                         )
        megaUnits = 1e6
        df['ping'] = df['ping_ms']
        df['downloadSpeed'] = df['downloadSpeed_bps'] / megaUnits
        df['uploadSpeed'] = df['uploadSpeed_bps'] / megaUnits
        df['Date'] = pd.to_datetime(df['t'],
                                infer_datetime_format=True)
        df.set_index('Date', drop=True, inplace=True)
        
        #collect some simple stats
        meanpg = df['ping'].mean()
        meandl = df['downloadSpeed'].mean()
        meanul = df['uploadSpeed'].mean()
        bestpg = df['ping'].min()
        bestdl = df['downloadSpeed'].max()
        bestul = df['uploadSpeed'].max()

        #begin plotting, plot U/D speed on left-y and ping on right-y
        fig, axL = plt.subplots()
        fig.canvas.set_window_title('SpeedTest Results')
        df.plot(y=['downloadSpeed','uploadSpeed'],
                ax=axL,
                marker='.',
                linestyle=''
                )
        plt.ylabel('Connection Speed (Mbps)')
        df.plot(y=['ping'],
                ax=axL,
                secondary_y=True,
                marker='.',
                linestyle='',
                color='r',
                alpha=0.6
                )
        plt.ylabel('Ping (ms)')
        plt.xlabel('Date')
        titleStr = 'Internet Connection Summary\n'
        titleStr += 'Average: ping={:0.1f}ms, DL={:0.2f}Mbps, UL={:0.2f}Mbps\n'
        titleStr += 'Best: ping={:0.1f}ms, DL={:0.2f}Mbps, UL={:0.2f}Mbps'
        titleStr = titleStr.format(meanpg, meandl, meanul, bestpg, bestdl, bestul)
        plt.title(titleStr)
#        figName = '/home/rcathriner/repos/isp_speed_tracker/connectionSummary.png'
        fig.savefig(figName, dpi=200, bbox_inches='tight')
 #       if showPlot:
 #           plt.show()

if __name__ == '__main__':
    fn = args.resultsFile #defaults to './testResults.csv'
    figfn = args.plotFile #defaults to './connectionSummary.png'
    spdTest = ISPSpeedTracker(fn)
    if not spdTest.csvIsInit():
        spdTest.initCSV()
    if not args.noTest:
        results = spdTest.runTest()
        spdTest.recordResults(results)
    spdTest.plotResults(figfn, args.showPlot)
