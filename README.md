# README #

This tool is used to provide logging and plotting of data measured using the speedtest-cli module ([link](https://github.com/sivel/speedtest-cli)). Typical usage would be rewriting the runTest.sh script to fit your system, and then inserting a call to runTest.sh into your crontab.